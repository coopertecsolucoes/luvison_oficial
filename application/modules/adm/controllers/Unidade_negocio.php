<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Unidade_negocio extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Unidade_negocio_model');
        $this->load->model('Usuario_grupo_model');
        $this->load->model('Unidade_negocio_foto_model');
        $this->load->helper('form');
    }

    public function index() {
        $data = new stdClass();
        //Título Página
        $data->titulo = 'Lista de Unidades de Negócios';
        //Cria as URL da View
        $data->caminho_url = caminhos_url_form('adm', 'unidade_negocio');
        $data->result = $this->Unidade_negocio_model->retorna_unidade_negocios();
        $this->load->template('unidade_negocio/index', $data);
    }

    public function create() {
        $data = new stdClass();
        //Título Página
        $data->titulo = 'Unidades de Negócios - Novo';
        //Cria as URL da View
        $data->caminho_url = caminhos_url_form('adm', 'unidade_negocio');
        //Busca os dados da View
        $data->grupo_usuarios = $this->Usuario_grupo_model->retorna_usuario_grupos();
        $this->load->template('unidade_negocio/create', $data);
    }

    public function edit($id) {
        $data = new stdClass();
        //Título Página
        $data->titulo = 'Unidades de Negócios';
        //Cria as URL da View
        $data->caminho_url = caminhos_url_form('adm', 'unidade_negocio');
        //Busca os dados da View
        //$data->grupo_usuarios = $this->Usuario_grupo_model->retorna_usuario_grupos();
        $data->result = $this->Unidade_negocio_model->retorna_unidade_negocio($id);
        $this->load->template('unidade_negocio/edit', $data);
    }

    public function create_registro() {
        $class = new stdClass();
        $class->id = $this->input->post('id');
        $class->ativo = 1;
        $class->nome_fantasia = $this->input->post('nome_fantasia');

        if ($this->Unidade_negocio_model->create_unidade_negocio($class)) {
            $retID = $this->db->insert_id();
            $this->session->set_flashdata('alerta_sucesso', $_SESSION['msg_sucesso']);
            redirect('adm/unidade_negocio/edit/' . $retID);
        } else {
            // user creation failed, this should never happen
            $this->session->set_flashdata('alerta_erro', $_SESSION['msg_erro']);
            redirect('adm/unidade_negocio/create');
        }
    }

    public function update_registro() {
        $class = new stdClass();
        $id = $this->input->post('id');
        $class->id = $this->input->post('id');
        $class->nome_fantasia = $this->input->post('nome_fantasia');
        $class->email = $this->input->post('email');
        $class->telefone = $this->input->post('telefone');
        $class->celular = $this->input->post('celular');
        $class->endereco = $this->input->post('endereco');
        $class->cidade = $this->input->post('cidade');
        $class->uf = $this->input->post('uf');
        $class->bairro = $this->input->post('bairro');
        $class->cep = $this->input->post('cep');
        $class->cnpj = $this->input->post('cnpj');
        $class->historia_titulo = $this->input->post('historia_titulo');
        $class->historia_descricao = $this->input->post('historia_descricao');
        $class->produto_titulo = $this->input->post('produto_titulo');
        $class->produto_descricao = $this->input->post('produto_descricao');
        $class->servico_titulo = $this->input->post('servico_titulo');
        $class->servico_descricao = $this->input->post('servico_descricao');
        $class->localizacao_titulo = $this->input->post('historia_titulo');
        $class->localizacao_descricao = $this->input->post('localizacao_descricao');
        $class->contato_titulo = $this->input->post('contato_titulo');
        $class->contato_descricao = $this->input->post('contato_descricao');
        $class->galeria_titulo = $this->input->post('galeria_titulo');
        $class->galeria_descricao = $this->input->post('galeria_descricao');
        $class->marca_titulo = $this->input->post('marca_titulo');
        $class->marca_descricao = $this->input->post('marca_descricao');
        $class->noticia_titulo = $this->input->post('noticia_titulo');
        $class->noticia_descricao = $this->input->post('noticia_descricao');
        $class->equipe_titulo = $this->input->post('equipe_titulo');
        $class->equipe_descricao = $this->input->post('equipe_descricao');
        $class->cliente_titulo = $this->input->post('cliente_titulo');
        $class->cliente_descricao = $this->input->post('cliente_descricao');

        $class->skype = $this->input->post('skype');
        $class->facebook = $this->input->post('facebook');
        $class->instagram = $this->input->post('instagram');
        $class->linkedin = $this->input->post('linkedin');
        $class->mapa = $this->input->post('mapa');
        $class->video = $this->input->post('video');

        $class->google_autor = $this->input->post('google_autor');
        $class->google_keywords = $this->input->post('google_keywords');
        $class->google_descricao = $this->input->post('google_descricao');
        $class->google_script = $this->input->post('google_script');
        $class->google_usuario = $this->input->post('google_usuario');
        $class->google_senha = $this->input->post('google_senha');
        $class->google_link = $this->input->post('google_link');

        $class->email_servidor = $this->input->post('email_servidor');
        $class->email_porta = $this->input->post('email_porta');
        $class->email_usuario = $this->input->post('email_usuario');
        $class->email_senha = $this->input->post('email_senha');

        //$class->usuario_grupo_id = $this->input->post('usuario_grupo_id');
        //UPLOAD DO LOGO
        if (!is_dir('./uploads/unidade_negocio/' . $id)) {
            mkdir('./uploads/unidade_negocio/' . $id, 0777);
        }

        // upload
        if (!empty($_FILES['logo']['name'])) {
            $config['upload_path'] = './uploads/unidade_negocio/' . $id;
            $config['allowed_types'] = 'jpg|png';
            $config['max_size'] = '10000';
            $config['overwrite'] = FALSE;
            $config['remove_spaces'] = TRUE;
            $new_name = normalizeChars($_FILES["logo"]['name']);
            $config['file_name'] = $new_name;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('logo')) {
                $data->error = $this->upload->display_errors();
                $this->session->set_flashdata('alerta_erro', $this->upload->display_errors());
                redirect('unidade_negocio/edit/' . $id);
            } else {
                $class->logo = 'uploads/unidade_negocio/' . $id . '/' . $this->upload->file_name;
            }
        }

        if (!is_dir('./uploads/unidade_negocio/' . $id . "/logo_secundario/")) {
            mkdir('./uploads/unidade_negocio/' . $id . "/logo_secundario/", 0777);
        }

        // upload
        if (!empty($_FILES['logo2']['name'])) {
            $config['upload_path'] = './uploads/unidade_negocio/' . $id . "/logo_secundario/";
            $config['allowed_types'] = 'jpg|png';
            $config['max_size'] = '10000';
            $config['overwrite'] = FALSE;
            $config['remove_spaces'] = TRUE;
            $new_name = normalizeChars($_FILES["logo2"]['name']);
            $config['file_name'] = $new_name;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('logo2')) {
                $data->error = $this->upload->display_errors();
                $this->session->set_flashdata('alerta_erro', $this->upload->display_errors());
                redirect('unidade_negocio/edit/' . $id);
            } else {
                $class->logo2 = './uploads/unidade_negocio/' . $id . "/logo_secundario/" . $this->upload->file_name;
            }
        }
        
        

        if ($this->Unidade_negocio_model->update_unidade_negocio($class)) {

            $this->session->set_flashdata('alerta_sucesso', $_SESSION['msg_sucesso']);
            redirect('adm/unidade_negocio/edit/' . $id);
        } else {
            // user creation failed, this should never happen
            $this->session->set_flashdata('alerta_erro', $_SESSION['msg_erro']);
            redirect('adm/unidade_negocio/create');
        }
    }

    public function ativar_registro($id) {
        $class = new stdClass();
        $class->id = $id;
        $class->ativo = $this->input->post('valor');

        if ($this->Unidade_negocio_model->update_unidade_negocio($class)) {
            $this->session->set_flashdata('alerta_sucesso', 'Salvo com sucesso!');
        }
    }

    public function delete($id) {

        // create the data object
        $data = new stdClass();

        if ($this->Unidade_negocio_model->delete_unidade_negocio($id)) {
            $this->session->set_flashdata('alerta_sucesso', $_SESSION['msg_sucesso']);
            redirect('adm/unidade_negocio/index');
        } else {


            $this->session->set_flashdata('alerta_erro', $_SESSION['msg_erro']);
            redirect('adm/unidade_negocio/index');
        }
    }

    public function imagens($id) {
        $data = new stdClass();
        $this->load->library('form_validation');
        $data->result = $this->Unidade_negocio_model->retorna_unidade_negocio($id);
        $data->fotos = $this->Unidade_negocio_foto_model->retorna_unidade_negocio_fotos($id);
        $this->load->template('adm/unidade_negocio/imagens', $data);
    }

    public function update_imagem() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->helper('geral');
        $this->load->library('form_validation');
        $id = $this->input->post('id');

      
            $class = new stdClass();
            if (!is_dir('./uploads/unidade_negocio_img/' . $id)) {
                mkdir('./uploads/unidade_negocio_img/' . $id, 0777);
            }

            // upload
            if (isset($_FILES['userfile'])) {
                $config['upload_path'] = './uploads/unidade_negocio_img/' . $id;
                $config['allowed_types'] = 'jpg|png';
                $config['max_size'] = '100000';
                $config['overwrite'] = FALSE;
                $config['remove_spaces'] = TRUE;
                $new_name = normalizeChars($_FILES["userfile"]['name']);
                $config['file_name'] = $new_name;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload()) {
                    $data->error = $this->upload->display_errors();
                    $this->session->set_flashdata('alerta_erro', $this->upload->display_errors());
                    redirect('adm/unidade_negocio/imagens/' . $id);
                } else {
                    $class->nome_arquivo = normalizeChars(basename($this->upload->file_name, $this->upload->file_ext));
                    $class->extensao = $this->upload->file_ext;
                    $this->load->library('image_lib', $config2);
                }
            }
            $class->unidade_negocio_id = $id;
            $class->principal = 0;
            if (!$this->Unidade_negocio_foto_model->create_unidade_negocio_foto($class)) {
                $data->error = 'Erro! Por favor, tente novamente.';
                $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
            }
            redirect('adm/unidade_negocio/imagens/' . $id);
   
    }

    public function ativa_principal($unidade_negocio_foto_id) {
        $data = new stdClass();
        $this->load->helper('form');
        $this->load->helper('geral');
        $this->load->library('form_validation');
        $foto = $this->Unidade_negocio_foto_model->retorna_unidade_negocio_foto($unidade_negocio_foto_id);

        $this->Unidade_negocio_foto_model->ativa_foto_principal($foto->unidade_negocio_id, $unidade_negocio_foto_id);
        redirect('adm/unidade_negocio/imagens/' . $foto->unidade_negocio_id);
    }

    public function delete_imagem($id) {
        
        $foto = $this->Unidade_negocio_foto_model->retorna_unidade_negocio_foto($id);
        if ($this->Unidade_negocio_foto_model->delete_unidade_negocio_foto($foto->id)) {
            unlink("./uploads/unidade_negocio_img/" . $foto->unidade_negocio_id . "/" . $foto->nome_arquivo . $foto->extensao);
            // OK
            $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
            redirect('adm/unidade_negocio/imagens/' . $foto->unidade_negocio_id);
        } else {
            // creation failed, this should never happen
            $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
            redirect('adm/unidade_negocio/imagens/' . $foto->unidade_negocio_id);
        }
    }

}
