<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unidade_federativa extends MX_Controller {
    
        public function __construct() {
            parent::__construct();
            $this->load->model('Unidade_federativa_model');
            $this->load->helper('form');
        }

	public function index(){
            $data = new stdClass();
            $data->titulo = 'Lista de Unidades Federativas';
            $this->load->template('unidade_federativa/index',$data);
	}
        
        public function edit($id){
            $data = new stdClass();
            $data->result = $this->Unidade_federativa_model->retorna_unidade_federativa($id);
            echo json_encode($data);
	}
        
        
        public function listagem() {
            $data = new stdClass();
            $data->result = $this->Unidade_federativa_model->retorna_unidade_federativas();
            $this->load->view('unidade_federativa/load_tabela', $data);
        }

        public function salvar_registro() {
            $class = new stdClass();
            $id = $this->input->post('id');
            if($id != "-1"){
                $class->id = $id;    
            }
            $class->descricao = $this->input->post('descricao');
            $class->sigla= strtoupper($this->input->post('sigla'));            
            $class->ativo = ($this->input->post('ativo') == 'on') ? 1 : 0;
            if ($this->Unidade_federativa_model->salvar($class)) {
                echo $_SESSION['msg_sucesso'];
            } else {
                echo $_SESSION['msg_erro'];
            }
        }
      
        public function ativar_registro($id){
            $class = new stdClass();
            $class->id = $id;    
            $class->ativo= $this->input->post('valor');                        
            $this->Unidade_federativa_model->salvar($class);
        }
        
        public function delete($id) {
            $data = new stdClass();
            if($this->permite_excluir($id)){
                $this->Unidade_federativa_model->delete($id);
                echo "";
            } else {
                echo $_SESSION["msg_excluir"];
            }
        }
        
        private function permite_excluir($uf_id){
            
            $result = new stdClass();
            $result = $this->Unidade_federativa_model->existe_cidade($uf_id);
            if (!empty($result)){
                return false;
            }
            return true;
        }
}
