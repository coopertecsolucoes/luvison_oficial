<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servico extends MX_Controller {
    
        public function __construct() {
            parent::__construct();
            $this->load->model('Servico_model');
            $this->load->model('Categoria_model');
            $this->load->model('Upload_model');
            $this->load->helper('form');
        }

	public function index(){
            $data = new stdClass();
            $data->titulo = 'Lista de Servicos';
            $data->categorias = $this->Categoria_model->retorna_categoria_ativos();
      
            $this->load->template('servico/index',$data);
	}
        
        public function listagem() {
            $data = new stdClass();
            $data->result = $this->Servico_model->retorna_servicos();
            $this->load->view('servico/load_tabela', $data);
        }
        
     

        public function edit($id){
            $data = new stdClass();
            $data->result = $this->Servico_model->retorna_servico($id);
            echo json_encode($data);
	}
        
        
        
        public function salvar_registro() {
            $class = new stdClass();
            $id = $this->input->post('id');
            if($id != "-1"){
                $class->id = $id;    
            }
            $class->nome = $this->input->post('nome');
            $class->descricao = $this->input->post('descricao');
            $class->categoria_id = $this->input->post('categoria_id');
            $class->tags = $this->input->post('tags');
            $class->preco = numbertodatabase($this->input->post('preco'));
            $class->seo_autor = $this->input->post('seo_autor');
            $class->seo_keywords = $this->input->post('seo_keywords');
            $class->seo_title = $this->input->post('seo_title');
            $class->seo_description = $this->input->post('seo_description');
            $class->ativo = ($this->input->post('ativo') == 'on') ? 1 : 0;
            if (!empty($_FILES['imagem']['name'])) {
                $class->banner = $this->Upload_model->upload_file($_FILES['imagem'], './uploads/servico','jpg|png|jpeg','imagem' );
            }  
            if ($this->Servico_model->salvar($class)) {
                
                echo $_SESSION['msg_sucesso'];
            } else {
                echo $_SESSION['msg_erro'];
            }
        }
      
        public function ativar_registro($id){
            $class = new stdClass();
            $class->id = $id;    
            $class->ativo= $this->input->post('valor');                        
            $this->Servico_model->salvar($class);
        }
        
        public function delete($id) {
            $data = new stdClass();
            if($this->permite_excluir($id)){
                $this->Servico_model->delete($id);
                echo "";
            } else {
                echo $_SESSION["msg_excluir"];
            }
        }
        
        private function permite_excluir($servico_id){
            
//            $result = new stdClass();
//            $result = $this->Servico_model->existe_servico($uf_id);
//            if (!empty($result)){
//                return false;
//            }
            return true;
        }
        
        
        
        
        
//          #####################################################
        
//           public function servico_fotos() {
//            $data = new stdClass();
//            $data->result = $this->Servico_model->retorna_servicos();
//            $this->load->view('servico/load_tabela', $data);
//        }
//        
        
        
        //#####################################################
        
//        public function listagem_avaliacao($servico_id) {
//            $data = new stdClass();
//        
//            $data->result = $this->Servico_match_model->retorna_servico_matchs($servico_id);
//            
//            $this->load->view('servico/load_match', $data);
//        }
//        
//        public function edita_match($id){
//            $data = new stdClass();
//            $data->result = $this->Servico_match_model->retorna_servico_match($id);
//            echo json_encode($data);
//	}
        
        
//        public function salvar_avaliacao() {
//            $class = new stdClass();
//            $id = $this->input->post('servico_match_id');
//            if($id != "-1"){
//                $class->id = $id;    
//            }
//            $class->servico_id = $this->input->post('servico_id');
//            $class->email = $this->input->post('email');
//            $class->avaliacao = $this->input->post('avaliacao');
//            $dt_data =DateTime::createFromFormat('d/m/Y',  $this->input->post('data'));
//            if ($dt_data == false) {
//                $date = null;
//            } else {
//                $date = $dt_data->format('Y-m-d');
//            }            
//            $class->data = empty($date) ? NULL : $date;  
//            if ($this->Servico_avaliacao_model->salvar($class)) {
//                
//                echo $_SESSION['msg_sucesso'];
//            } else {
//                echo $_SESSION['msg_erro'];
//            }
//        }
//        
//         public function ativar_match($id){
//            $class = new stdClass();
//            $class->id = $id;    
//            $class->ativo= $this->input->post('valor');                        
//            $this->Servico_match_model->salvar($class);
//        }
        
//        public function delete_avaliacao($id) {
//            $data = new stdClass();
//            $this->Servico_avaliacao_model->delete($id);
//            echo "";
//           
//        }
        
}
