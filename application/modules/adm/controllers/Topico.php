<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Topico extends MX_Controller {
    
        public function __construct() {
            parent::__construct();
            $this->load->model('Topico_model');
//            $this->load->model('Marca_model');
//            $this->load->model('Topico_foto_model');
            $this->load->model('Upload_model');
            $this->load->helper('form');
        }

	public function index(){
            $data = new stdClass();
            $data->titulo = 'Lista de Topicos';

      
            $this->load->template('topico/index',$data);
	}
        
        public function listagem() {
            $data = new stdClass();
            $data->result = $this->Topico_model->retorna_topicos();
            $this->load->view('topico/load_tabela', $data);
        }
        
     

        public function edit($id){
            $data = new stdClass();
            $data->result = $this->Topico_model->retorna_topico($id);
//            $data->fotos = $this->Topico_foto_model->retorna_topico_fotos($id);
            echo json_encode($data);
	}
        
        
        
        public function salvar_registro() {
            $class = new stdClass();
            $id = $this->input->post('id');
            if($id != "-1"){
                $class->id = $id;    
            }
            $class->texto = $this->input->post('texto');
            $class->descricao = $this->input->post('descricao');
            $class->icone = $this->input->post('icone');
            $class->ordem = $this->input->post('ordem');
            $class->ativo = ($this->input->post('ativo') == 'on') ? 1 : 0;
            if (!empty($_FILES['file_banner']['name'])) {
            $class->imagem = $this->Upload_model->upload_file($_FILES['file_banner'], './uploads/topico','jpg|png|jpeg','file_banner' );
            }  
            if ($this->Topico_model->salvar($class)) {
                
                echo $_SESSION['msg_sucesso'];
            } else {
                echo $_SESSION['msg_erro'];
            }
        }
      
        public function ativar_registro($id){
            $class = new stdClass();
            $class->id = $id;    
            $class->ativo= $this->input->post('valor');                        
            $this->Topico_model->salvar($class);
        }
        
        public function delete($id) {
            $data = new stdClass();
            if($this->permite_excluir($id)){
                $this->Topico_model->delete($id);
                echo "";
            } else {
                echo $_SESSION["msg_excluir"];
            }
        }
        
        private function permite_excluir($topico_id){
            
//            $result = new stdClass();
//            $result = $this->Topico_model->existe_topico($uf_id);
//            if (!empty($result)){
//                return false;
//            }
            return true;
        }
        
        
        
        
        
//          #####################################################
        
        
        
//        public function topico_fotos($id) {
//            $data = new stdClass();
//            $data->fotos = $this->Topico_foto_model->retorna_topico_fotos($id);
//            $this->load->view('topico/load_fotos', $data);
//        }
//        
//        
//        public function upload_foto(){
//            $class = new stdClass();
//           if (!empty($_FILES['file_foto']['name'])) {
//            $class->foto = $this->Upload_model->upload_file($_FILES['file_foto'], './uploads/topico','jpg|png', 'file_foto' );
//            $class->topico_id= $this->input->post('topico_id');
//             $this->Topico_foto_model->salvar($class);
//        }  
//            
//     }       
//            
//           public function delete_foto($topico_foto_id) {
//            $data = new stdClass();
//            
//                $this->Topico_foto_model->delete($topico_foto_id);
//                echo "";
//                
//        }
    
   
        
        
        //#####################################################
        
//        public function listagem_avaliacaos($topico_id) {
//            $data = new stdClass();
//        
//            $data->result = $this->Topico_avaliacao_model->retorna_topico_avaliacaos($topico_id);
//            
//            $this->load->view('topico/load_avaliacao', $data);
//        }
//        
//        public function edita_avaliacao($id){
//            $data = new stdClass();
//            $data->result = $this->Topico_avaliacao_model->retorna_topico_avaliacao($id);
//            echo json_encode($data);
//	}
        
        
//        public function salvar_avaliacao() {
//            $class = new stdClass();
//            $id = $this->input->post('topico_id');
//            if($id != "-1"){
//                $class->id = $id;    
//            }
//            $class->topico_id = $this->input->post('topico_id');
//            $class->email = $this->input->post('email');
//            $class->avaliacao = $this->input->post('avaliacao');
//            $dt_data =DateTime::createFromFormat('d/m/Y',  $this->input->post('data'));
//            if ($dt_data == false) {
//                $date = null;
//            } else {
//                $date = $dt_data->format('Y-m-d');
//            }            
//            $class->data = empty($date) ? NULL : $date;  
//            if ($this->Topico_avaliacao_model->salvar($class)) {
//                
//                echo $_SESSION['msg_sucesso'];
//            } else {
//                echo $_SESSION['msg_erro'];
//            }
//        }
//        
//         public function ativar_avaliacao($id){
//            $class = new stdClass();
//            $class->id = $id;    
//            $class->ativo= $this->input->post('valor');                        
//            $this->Topico_avaliacao_model->salvar($class);
//        }
//        
//        public function delete_avaliacao($id) {
//            $data = new stdClass();
//            $this->Topico_avaliacao_model->delete($id);
//            echo "";
//           
//        }
        
}
