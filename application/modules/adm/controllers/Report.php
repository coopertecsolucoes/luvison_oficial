<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Categorias class.
 * 
 * @extends CI_Controller
 */
class Report extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
        $this->load->model('Report_model');
         //$this->load->library('PHPJasperXML');
         //$this->load->library('PHPJasperXMLSubReport');
    }

    
    
    public function rel_acessos() {
         $data = new stdClass();
         $this->load->model('Usuario_model');
         $data->result->data_inicial = date("Y") . '/' . date("m") . '/01';
         $data->result->data_final = date("Y-m-t");
         $data->usuarios = $this->Usuario_model->retorna_usuarios();
         $this->load->template('relatorios/acessos', $data);
    }
    
    public function gera_rel_acessos() {
        $data = new stdClass();
        $usuario_id =(int)$this->input->post('usuario_id');
        //$data_inicial = date("Y-m-d",  $this->input->post('data_inicial'));
        //$data_final =  date("Y-m-d", $this->input->post('data_final'));

        $dt =DateTime::createFromFormat('d/m/Y',  $this->input->post('data_inicial'));
        if ($dt == false) {
            $date = null;
        } else {
            $date = $dt->format('Y-m-d');
        }            
        $data_inicial = empty($date) ? NULL : $date;
        $dt =DateTime::createFromFormat('d/m/Y',  $this->input->post('data_final'));
        if ($dt == false) {
            $date = null;
        } else {
            $date = $dt->format('Y-m-d');
        }            
        $data_final = empty($date) ? NULL : $date;
        $tipo_rel =(int)$this->input->post('tipo_rel');
        //$Parametros=array("pessoa_id"=>(int)$pessoa_id);
        $Parametros=array("usuario_id"=>(int)$usuario_id, "data_inicial"=>$data_inicial, "data_final"=>$data_final);
        if ($tipo_rel==0)
        {
            $data->result =$this->Report_model->Gera_Relatorio($Parametros,"listagem_acesso");
        }
        else {
            $data->result =$this->Report_model->Gera_Relatorio($Parametros,"listagem_acesso_usuario_data");
        }
        //$this->load->template('relatorios/acessos', $data);
         
    }
 

   

}
