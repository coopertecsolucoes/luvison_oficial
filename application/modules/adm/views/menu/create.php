<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

    <div class="row">
        <div class="col-12">
          

            <div class="card mb-4">
               
                <div class="card-body">
                <div class="card-title mb-4">
                    <h4><?php echo $titulo?></h4>
                </div>
                <div class="card-subtitle">


                </div>
                    <form method="POST" id="validation-form" novalidate="novalidate" data-select2-id="validation-form" 
                          action="<?php echo $caminho_url->create_registro?>" class="col-md-10 col-sm-10 col-xs-12">    
                        <div class="form-row">
                            <fieldset class="form-group col-md-6">
                                <label class="form-label">Tipo Menu</label>
                                <select id="tipo_menu_id" name="tipo_menu_id" class="custom-select">
                                  <option>Selecione...</option>
                                    <?php foreach ($tipo_menus as $tipo) { ?>
                                        <option value="<?php echo $tipo->id ?>"><?php echo $tipo->descricao ?></option>
                                    <?php } ?>
                                </select>
                            </fieldset>
                            <fieldset class="form-group col-md-6">
                              <label class="form-label">Descrição</label>
                              <input type="text" id="descricao" name="descricao" required class="form-control">
                            </fieldset>
                        </div>    
                        <div class="form-row">
                            <fieldset class="form-group col-md-6">
                             <label class="form-label">Menu</label>
                             <input type="text" id="menu" name="menu" required class="form-control">
                           </fieldset>
                           <fieldset class="form-group col-md-4">
                             <label class="form-label">Ícone</label>
                             <input type="text" id="icone" name="icone" required class="form-control">
                           </fieldset>
                           <fieldset class="form-group col-md-2">
                             <label class="form-label">Ordem</label>
                             <input type="text" id="ordem" name="ordem"  required class="form-control">
                           </fieldset>
                        </div>    
                        <div class="form-row">
                            <fieldset class="form-group col-md-3">
                                <label class="form-label">Mostrar Paínel</label>
                                <select id="mostra_painel" name="mostra_painel" class="custom-select">
                                    <option value="0" >Não</option>
                                    <option value="1" >Sim</option>                                
                                </select>
                            </fieldset>
                            <fieldset class="form-group col-md-3">
                                <label class="form-label">Destino Página</label>
                                <select id="target" name="target" class="custom-select">
                                    <option value="_self" >Mesma Guia</option>
                                    <option value="_black" >Nova Guia</option>                                
                                </select>
                            </fieldset>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success submit-btn with-arrow  mr-2 mb-4">Salvar</button>
                            <a href="<?php echo $caminho_url->index ?>" class="btn mr-2 mb-4">Voltar</a>
                        </div>    
              </form>
                     
            </div>
            </div>
        </div>

    </div>
