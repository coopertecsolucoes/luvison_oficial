    <?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script src="<?php echo base_url('assets/js/cadastros/helper.js')?>"></script>
<script src="<?php echo base_url('assets/js/cadastros/contato.js')?>"></script>


<div class="container-fluid">
    <section class="py-5">
        <div class="row">
            <div class="col-lg-12 mb-4">
                <div class="card">
                    <div class="card-header" style="display:flex">
                        <div class="col-md-10">  
                            <h6 class="text-uppercase mb-0 mt-1"><?php echo $titulo ?></h6>
                        </div>
                        <div class="col-md-2">
                            <a onclick="novo()"  class="btn btn-primary btn-sm" tabindex="0"><i class="fa fa-newspaper-o"></i>Novo</a>
                        </div>
                    </div>
                    <div class="card-body" id="listagem">
                        <!-- Carrega a listagem via js -->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal" tabindex="-1" role="dialog" id="modal-dados">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="titulo_modal">Dados</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="frm_modal"  method="POST"  enctype="multipart/form-data" class="needs-validation" novalidate>
                    <ul class="nav nav-tabs" id="myTab" role="">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Dados</a>
                        </li>
                    </ul>

                    <div class="tab-content" id="myTabContent">

                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                            <input hidden type="text" name="id" id="id" value="-1">
                            <div class="form-row mt-3">
                                <div class="form-group col-md-12">
                                    <label class="form-control-label"><span class="text-danger"><i class="fa fa-star"></i></span>Nome</label>
                                    <input type="text"  name="nome" id="nome" class="form-control" required>
                                    <div class="invalid-feedback">Favor preencher o campo!</div>
                                </div>
                                
                                <div class="form-group col-md-12">
                                    <label class="form-control-label"><span class="text-danger"><i class="fa fa-star"></i></span>Email</label>
                                    <input type="text"  name="email" id="email" class="form-control" required>
                                    <div class="invalid-feedback">Favor preencher o campo!</div>
                                </div>
                                
                                <div class="form-group col-md-12">
                                    <label class="form-control-label"><span class="text-danger"><i class="fa fa-star"></i></span>Assunto</label>
                                    <textarea  type="text"  name="assunto" id="assunto" class="form-control" ></textarea> 
                                    <div class="invalid-feedback">Favor preencher o campo!</div>
                                </div>
                                
                                <div class="form-group col-md-12">
                                    <label class="form-control-label"><span class="text-danger"><i class="fa fa-star"></i></span>Mensagem</label>
                                    <textarea type="text"  name="mensagem" id="mensagem" class="form-control" ></textarea> 
                                    <div class="invalid-feedback">Favor preencher o campo!</div>
                                </div>
                                
                                <div class="form-group col-md-12">
                                    <label class="form-control-label"><span class="text-danger"><i class="fa fa-star"></i></span>Data</label>
                                    <input readonly type="text"  name="data" id="data" class="form-control" required>
                                    <div class="invalid-feedback">Favor preencher o campo!</div>
                                </div>
                            </div>
                            <div class="form-group">       
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                <a onclick="salvar()" id="btnsalvar" tabindex="99" class="btn btn-success">Salvar</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>







<script type="text/javascript">
 
    var path = '<?php echo base_url() ?>';

    $(document).ready(function () {
         
            carrega_registros();
//             $('#telefone').mask("####0,0", {reverse: true});
             $('#telefone').mask("(99) 9999-99999");
             $('#celular').mask("(99) 9 9999-9999");
             $('#cep').mask("99999-999");
             $('#cnpj').mask("99999999/9999-99");

              $('#view_report_table').DataTable({
                responsive: true,
                dom: 'lTf<"html5buttons"B>gtip',
                pageLength: 25,
                buttons: ['excel', 'pdf'],
                language: {
                    url: '<?php echo base_url("assets/vendor/data-table/js/portugues.json"); ?>'
                }
            });
            
    });
    


</script>