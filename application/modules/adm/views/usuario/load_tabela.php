    <?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<table  class="table hover" id="view_report_table" cellspacing="0" width="100%">
    <thead >
        <tr>
            <th>Descrição</th>
            <th>Ativo</th>
            <th>Ações</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (isset($result)) {
            foreach ($result as $row) {
                if ($row->ativo == 1) {
                    $ativo = 'Sim';
                } else {
                    $ativo = 'Não';
                }
                ?>
                <tr>
                    <td>
                        <a onclick="editar('<?php echo  $row->id ?>')">
                        <?php echo $row->nome; ?>
                        </a>
                    </td>
                    <td style="width: 10%"> 
                        <label class="switcher"  >
                            <input type="checkbox" id="<?php echo "ativo_" . $row->id ?>" onchange="ativar_registro()" name="<?php echo "ativo_" . $row->id ?>" <?php echo ($row->ativo === '1') ? 'checked' : ''; ?> class="switcher-input">
                            <span class="switcher-indicator">
                            </span>
                        </label>
                    </td>
                    <td class="text-white" style="text-align: center; width: 25%">
                        <a title="Editar Registro" onclick="editar('<?php echo  $row->id ?>')" class="btn btn-primary btn-sm"  tabindex="0"><i class="fa fa-pencil-square-o"></i></a>
                        <a title="Excluir Registro" onclick="excluir('<?php echo $row->id ?>')" class="btn btn-danger btn-sm"  tabindex="0"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table>

<script type="text/javascript">
    $(document).ready(function () {
         

              $('#view_report_table').DataTable({
                responsive: true,
                dom: 'lTf<"html5buttons"B>gtip',
                pageLength: 25,
                buttons: ['excel'],
                language: {
                    url: '<?php echo base_url("assets/vendor/data-table/js/portugues.json"); ?>'
                }
            });
       
      
      
    });
    
    </script>