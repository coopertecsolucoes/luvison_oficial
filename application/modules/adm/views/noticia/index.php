    <?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script src="<?php echo base_url('assets/js/cadastros/helper.js')?>"></script>
<script src="<?php echo base_url('assets/js/cadastros/noticia.js')?>"></script>


<div class="container">
    <section class="py-5">
            <div class="row">
              <div class="col-lg-12 mb-4">
                <div class="card">
                  <div class="card-header" style="display:flex">
                    <div class="col-md-10">  
                        <h6 class="text-uppercase mb-0 mt-1"><?php echo $titulo?></h6>
                    </div>
                    <div class="col-md-2">
                        <a onclick="novo()"  class="btn btn-primary btn-sm" tabindex="0"><i class="fa fa-newspaper-o"></i> Novo</a>
                    </div>
                  </div>
                  <div class="card-body" id="listagem">
                    <!-- Carrega a listagem via js -->
                  </div>
                </div>
              </div>
            </div>
    </section>

</div>
<div class="modal" tabindex="-1" role="dialog" id="modal-dados">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="titulo_modal">Dados</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Dados</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">

                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <form id="frm_modal"  method="POST"  enctype="multipart/form-data" class="needs-validation" novalidate>

                            <input hidden type="text" name="id" id="id" value="-1">
                            <div class="form-row mt-3">
                                <div class="form-group col-md-6">
                                    <label class="form-control-label"><span class="text-danger"></span> Data</label>
                                    <input type="text"  name="data" id="data" class="form-control">
                                    <div class="invalid-feedback">Favor preencher o campo!</div>
                                </div>
                                
                                <div class="form-group col-md-6">
                                    <label class="form-control-label"><span class="text-danger"><i class="fa fa-star"></i></span> Título</label>
                                    <input type="text"  name="titulo" id="titulo" class="form-control" required>
                                    <div class="invalid-feedback">Favor preencher o campo!</div>
                                </div>
                                
                                <div class="form-group col-md-12">
                                    <label class="form-control-label"><span class="text-danger"><i class="fa fa-star"></i></span> Notícia</label>
                                    <textarea type="text" name="noticia" id="noticia" rows="5" class="form-control" required></textarea>
                                    <div class="invalid-feedback">Favor preencher o campo!</div>
                                </div>
                                
                                <div class="form-group col-md-6">
                                    <label class="form-control-label"><span class="text-danger"><i class="fa fa-star"></i></span> Descrição Fonte</label>
                                    <input type="text"  name="descricao_fonte" id="descricao_fonte" class="form-control" required>
                                    <div class="invalid-feedback">Favor preencher o campo!</div>
                                </div>
                                
                                <div class="form-group col-md-6">
                                    <label class="form-control-label"><span class="text-danger"><i class="fa fa-star"></i></span> Link da Fonte</label>
                                    <input type="text"  name="link_fonte" id="link_fonte" class="form-control" required>
                                    <div class="invalid-feedback">Favor preencher o campo!</div>
                                </div>
                                
                                <div class="form-group col-md-6">
                                    <label class="form-control-label"><span class="text-danger"><i class="fa fa-star"></i></span>Imagem</label><br>
                                    <input type="file" id="file_banner_noticia" name="file_banner_noticia" >
                                </div>
                                
                                <div class="form-group col-md-6">
                                    <div class="custom-control custom-checkbox">
                                        <input id="ativo" name="ativo" type="checkbox" checked class="custom-control-input">
                                        <label for="ativo" class="custom-control-label">Ativo</label>
                                    </div>
                                </div>
                            </div>
  
                            <div class="form-group">       
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                <a onclick="salvar()" tabindex="99" class="btn btn-success">Salvar</a>
                            </div>
                                    
                        </form>
                    </div>

                    
                </div>
                
               
       
      </div>

    </div>
  </div>
</div>

<link href="<?php echo base_url("assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css") ?>" />
<script src="<?php echo base_url("assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js") ?>"></script>


<script type="text/javascript">
 
    var path = '<?php echo base_url() ?>';

    $(document).ready(function () {
         
            carrega_registros();

              $('#view_report_table').DataTable({
                responsive: true,
                dom: 'lTf<"html5buttons"B>gtip',
                pageLength: 25,
                buttons: ['excel', 'pdf'],
                language: {
                    url: '<?php echo base_url("assets/vendor/data-table/js/portugues.json"); ?>'
                }
            });
            
        $('#data').datepicker({
            format: "dd/mm/yyyy",
            language: "pt-BR",
            todayHighlight: true,
            autoclose: true
        });
       
      
      
    });
    


</script>