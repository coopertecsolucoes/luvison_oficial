    <?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script src="<?php echo base_url('assets/js/cadastros/helper.js')?>"></script>
<script src="<?php echo base_url('assets/js/cadastros/banner.js')?>"></script>



<div class="container">
    <section class="py-5">
        <div class="row">
            <div class="col-lg-12 mb-4">
                <div class="card">
                    <div class="card-header" style="display:flex">
                        <div class="col-md-10">  
                            <h6 class="text-uppercase mb-0 mt-1"><?php echo $titulo ?></h6>
                        </div>
                        <div class="col-md-2">
                            <a onclick="novo()"  class="btn btn-primary btn-sm" tabindex="0"><i class="fa fa-newspaper-o"></i> Novo</a>
                        </div>
                    </div>
                    <div class="card-body" id="listagem">
                        <!-- Carrega a listagem via js -->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal" tabindex="-1" role="dialog" id="modal-dados">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="titulo_modal">Dados</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <form id="frm_modal"  method="POST"  enctype="multipart/form-data" class="needs-validation" novalidate>
                <ul class="nav nav-tabs" id="myTab" role="">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Dados</a>
                    </li>
<!--                    <li class="nav-item">
                        <a class="nav-link " id="test-tab" data-toggle="tab" href="#seo" role="tab" aria-controls="test" aria-selected="true">Seo</a>
                    </li>-->
<!--                    <li class="nav-item">
                        <a class="nav-link " id="test-tab" data-toggle="tab"  href="#foto" role="tab" aria-controls="test" aria-selected="true">Fotos</a>
                    </li>-->
                </ul>

                <div class="tab-content" id="myTabContent">

                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        

                            <input hidden type="text" name="id" id="id" value="-1">
                            <div class="form-row mt-3">

                                <div class="form-group col-md-12">
                                    <label class="form-control-label"><span class="text-danger"></span>Texto</label>
                                    <input type="text"  name="texto" id="texto" class="form-control">
                                    <div class="invalid-feedback">Favor preencher o campo!</div>
                                </div>
                                
                                <div class="form-group col-md-12">
                                    <label class="form-control-label"><span class="text-danger"></span>Descrição interna</label>
                                    <textarea type="text"  name="descricao_interna" id="descricao_interna" class="form-control" ></textarea> 
                                    <div class="invalid-feedback">Favor preencher o campo!</div>
                                </div>
                              
                                <div class="form-group col-md-12">
                                    <label class="form-control-label"><span class="text-danger"></span>Link</label>
                                    <input type="text"  name="link" id="link" class="form-control">
                                    <div class="invalid-feedback">Favor preencher o campo!</div>
                                </div>
                                
                                <div class="form-group col-md-12">
                                    <label class="form-control-label"><span class="text-danger"></span>Link Descrição</label>
                                    <textarea type="text"  name="desc_link" id="desc_link" class="form-control" ></textarea> 
                                    <div class="invalid-feedback">Favor preencher o campo!</div>
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="form-control-label"><span class="text-danger"><i class="fa fa-star"></i></span>Ordem</label>
                                    <input type="text"  name="ordem" id="ordem" class="form-control" required>
                                    <div class="invalid-feedback">Favor preencher o campo!</div>
                                </div>
                                
                                <div class="form-group col-md-12">
                                    <label class="form-control-label"><span class="text-danger"><i class="fa fa-star"></i></span>Imagem</label><br>
                                    <input type="file" id="file_banner" name="file_banner" >
                                </div>

                                <div class="form-group col-md-12">
                                    <div class="custom-control custom-checkbox">
                                        <input id="ativo" name="ativo" type="checkbox" checked class="custom-control-input">
                                        <label for="ativo" class="custom-control-label">Ativo</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">       
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                <a onclick="salvar()" id="btnsalvar" tabindex="99" class="btn btn-success">Salvar</a>
                            </div>

                        
                    </div>

<!--                    <div class="tab-pane fade show" id="seo" role="tabpanel" aria-labelledby="seo">
                      

                
                            <div class="form-row mt-3">

                                <div class="form-group col-md-12">
                                    <label class="form-control-label">SEO Autor</label>
                                    <input type="text"  name="seo_autor" id="seo_autor" class="form-control" required>
                                    <div class="invalid-feedback">Favor preencher o campo!</div>
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="form-control-label">SEO Title</label>
                                    <input type="text"  name="seo_title" id="seo_title" class="form-control" required>
                                    <div class="invalid-feedback">Favor preencher o campo!</div>
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="form-control-label">SEO Keywords</label>
                                    <input type="text"  name="seo_keywords" id="seo_keywords" class="form-control" required>
                                    <div class="invalid-feedback">Favor preencher o campo!</div>
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="form-control-label">SEO Description</label>
                                    <textarea type="text"  name="seo_description" id="seo_description" class="form-control" ></textarea> 
                                    <div class="invalid-feedback">Favor preencher o campo!</div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="custom-control custom-checkbox">
                                        <input id="ativo" name="ativo" type="checkbox" checked class="custom-control-input">
                                        <label for="ativo" class="custom-control-label">Ativo</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">       
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                <a onclick="salvar()" id="btnsalvar" tabindex="99" class="btn btn-success">Salvar</a>
                            </div>

                       
                    </div>-->
                       
<!--                         <div class="tab-pane fade show" id="foto" role="tabpanel" aria-labelledby="foto">
                             
                         
                             
                               <div class=container >
                                    <div class="row text-center text-lg-left" id="banner_fotos">
                                       

                                    </div>
                                </div>
                             
                             
                            <div class="form-group">       
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                
                            </div>

                       
                    </div>-->
                </div>

            </form>

            </div>

        </div>
    </div>
</div>



<!--<div class="modal" tabindex="-1" role="dialog" id="modal_avaliacao">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="titulo_modal">Avaliação</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="lista_tab" onclick="inicializa_avaliacao()" data-toggle="tab" href="#lista" role="tab" aria-controls="home" aria-selected="true">Lista</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="match_tab" data-toggle="tab" href="#tab_avaliacao" role="tab" aria-controls="home" aria-selected="true">Adicionar</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">

                    <div class="tab-pane fade show active" id="lista" role="tabpanel" aria-labelledby="home-tab">
                        
                        
                    </div>

                    <div class="tab-pane fade show" id="tab_match" role="tabpanel" aria-labelledby="home-tab">
                       <form id="frm_match"  method="POST"  enctype="multipart/form-data" class="needs-validation" novalidate>

                       <input hidden type="text" name="banner_id" id="banner_id" value="-1">
                         
  
                          
                        </form>
                    </div>

                    
                </div>
                
               
       
      </div>

    </div>
  </div>
</div>-->



<script type="text/javascript">
 
    var path = '<?php echo base_url() ?>';

    $(document).ready(function () {
         
            carrega_registros();
//             $('#telefone').mask("####0,0", {reverse: true});
             $('#telefone').mask("(99) 9999-99999");
             $('#celular').mask("(99) 9 9999-9999");
             $('#cep').mask("99999-999");
             $('#cnpj').mask("99999999/9999-99");

              $('#view_report_table').DataTable({
                responsive: true,
                dom: 'lTf<"html5buttons"B>gtip',
                pageLength: 25,
                buttons: ['excel', 'pdf'],
                language: {
                    url: '<?php echo base_url("assets/vendor/data-table/js/portugues.json"); ?>'
                }
            });
            


      
    });
    


</script>