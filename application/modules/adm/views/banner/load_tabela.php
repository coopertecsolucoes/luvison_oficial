    <?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<table  class="table hover" id="tabela_load" cellspacing="0" width="100%">
    <thead >
        <tr>
            <th>Texto</th>
            <th>Ordem</th>
            <th>Ativo</th>
            <th>Ações</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (isset($result)) {
            foreach ($result as $row) {
                if ($row->ativo == 1) {
                    $ativo = 'Sim';
                } else {
                    $ativo = 'Não';
                }
                ?>
                <tr>
                    <td>
                        <a onclick="editar('<?php echo  $row->id ?>')">
                        <?php echo $row->texto?>
                        </a>
                    </td>
                    <td>
                        <?php echo $row->ordem?>
                    </td>
                   
                    <td style="width: 10%"> 

                        <label class="switcher"  >
                            <input type="checkbox" id="<?php echo "ativo_" . $row->id ?>" onchange="ativar_registro('<?php echo $row->id ?>')" name="<?php echo "ativo_" . $row->id ?>" <?php echo ($row->ativo === '1') ? 'checked' : ''; ?> class="switcher-input">
                            <span class="switcher-indicator">
                            </span>
                        </label>
                    </td>
                    <td class="text-white" style="text-align: center; width: 25%">

                        <a title="Editar Registro" onclick="editar('<?php echo  $row->id ?>')" class="btn btn-primary btn-sm"  tabindex="0"><i class="fa fa-pencil-square-o"></i></a>
                        <a title="Excluir Registro" onclick="excluir('<?php echo $row->id ?>')" class="btn btn-danger btn-sm"  tabindex="0"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table>

<script type="text/javascript">
    $(document).ready(function () {
         

            $('#tabela_load').DataTable({
                responsive: true,
                ordering: true,    
                paging: false,
                pageLength: 25,
                searching: false,
                info: false,                
                dom: 'lTf<"html5buttons"B>gtip',                
                buttons: [],
                language: {
                    url: '<?php echo base_url("assets/vendor/data-table/js/portugues.json"); ?>'
                }
            });
       
      
      
    });
    
    </script>