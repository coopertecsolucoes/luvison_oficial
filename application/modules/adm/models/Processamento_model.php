<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Serie_model class.
 * 
 * @extends CI_Model
 */
class Processamento_model extends CI_Model {

    public function __construct() {
        parent::__construct();        
    }

    public function retorna_processamentos() {
        
        $this->db->from('processamento');
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }
    

    public function salvar($data) {
        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('processamento', $data);
        } else {
            return $this->db->insert('processamento', $data);
        }
    }

    public function delete($id) {
        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('processamento');
        }
    }

    public function retorna_processamento($id) {

        $this->db->from('processamento');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_processamento_ativos() {
        
        $this->db->from('processamento');
        $this->db->where('ativo', 1);
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function existe_processamento($uf_id) {
        $this->db->from('processamento');
        $this->db->where('processamento_id', $uf_id);                
        $query = $this->db->get();
        return $query->result();
    }
    

}
