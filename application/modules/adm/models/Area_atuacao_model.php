<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Serie_model class.
 * 
 * @extends CI_Model
 */
class Area_atuacao_model extends CI_Model {

    public function __construct() {
        parent::__construct();        
    }

    public function retorna_area_atuacaos() {
        
        $this->db->from('area_atuacao');
        $this->db->order_by('ordem');
        $query = $this->db->get();
        return $query->result();
    }
    

    public function salvar($data) {
        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('area_atuacao', $data);
        } else {
            return $this->db->insert('area_atuacao', $data);
        }
    }

    public function delete($id) {
        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('area_atuacao');
        }
    }

    public function retorna_area_atuacao($id) {

        $this->db->from('area_atuacao');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_area_atuacao_ativos() {
        
        $this->db->from('area_atuacao');
        $this->db->where('ativo', 1);
        $this->db->order_by('ordem');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function existe_area_atuacao($uf_id) {
        $this->db->from('area_atuacao');
        $this->db->where('area_atuacao_id', $uf_id);                
        $query = $this->db->get();
        return $query->result();
    }
    
    public function retorna_area_atuacao_site($categoria_id) {
        
        $this->db->from('area_atuacao a');
        $this->db->where('EXISTS(SELECT * FROM produto pro join categoria ca ON ca.id=pro.categoria_id WHERE pro.area_atuacao_id=a.id AND ca.id='. $categoria_id .')');
        $this->db->where('ativo', 1);
        $this->db->order_by('ordem');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function retorna_area_atuacao_componente($categoria_id) {
        
        $this->db->from('area_atuacao a');
        $this->db->where('EXISTS(SELECT * FROM componente comp join categoria ca ON ca.id=comp.categoria_id WHERE comp.area_atuacao_id=a.id AND ca.id='. $categoria_id .')');
        $this->db->where('ativo', 1);
        $this->db->order_by('ordem');
        $query = $this->db->get();
        return $query->result();
    }

}
