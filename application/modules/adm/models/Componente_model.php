<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Serie_model class.
 * 
 * @extends CI_Model
 */
class Componente_model extends CI_Model {

    public function __construct() {
        parent::__construct();        
    }

    public function retorna_componentes() {
        $this->db->select('co.*, ca.descricao desc_categoria, at.descricao desc_area');
        $this->db->from('componente co');
        $this->db->join('categoria ca','co.categoria_id=ca.id');
        $this->db->join('area_atuacao at','co.area_atuacao_id=at.id');
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }
    

    public function salvar($data) {
        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('componente', $data);
        } else {
            return $this->db->insert('componente', $data);
        }
    }

    public function delete($id) {
        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('componente');
        }
    }

    public function retorna_componente($id) {

        $this->db->from('componente');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_componente_ativos() {
        
        $this->db->from('componente');
        $this->db->where('ativo', 1);
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function existe_componente($uf_id) {
        $this->db->from('componente');
        $this->db->where('componente_id', $uf_id);                
        $query = $this->db->get();
        return $query->result();
    }
    
    public function retorna_componentes_site($categoria_id) {
        $this->db->select('co.*, ca.descricao desc_categoria, at.descricao desc_area, at.desc_filtro');
        $this->db->from('componente co');
        $this->db->join('categoria ca','co.categoria_id=ca.id');
        $this->db->join('area_atuacao at','co.area_atuacao_id=at.id');
        $this->db->where('co.categoria_id', $categoria_id);
        $this->db->order_by('co.descricao');
        $query = $this->db->get();
        return $query->result();
    }
    

}
