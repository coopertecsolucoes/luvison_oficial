<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Serie_model class.
 * 
 * @extends CI_Model
 */
class Empresa_model extends CI_Model {

    public function __construct() {
        parent::__construct();        
    }

    public function retorna_empresas() {
        $this->db->select('e.*, cid.descricao desc_cidade, uf.id unidade_federativa_id, uf.sigla, uf.descricao desc_uf');
        $this->db->from('empresa e');
        $this->db->join('cidade cid','cid.id=e.cidade_id');
        $this->db->join('unidade_federativa uf','cid.unidade_federativa_id=uf.id');
        $this->db->order_by('e.nome_fantasia');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function salvar($data) {
        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('empresa', $data);
        } else {
            return $this->db->insert('empresa', $data);
        }
    }

    public function delete($id) {
        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('empresa');
        }
    }

    public function retorna_empresa($id) {

        $this->db->select('e.*, cid.descricao desc_cidade, uf.id unidade_federativa_id, uf.sigla, uf.descricao desc_uf');
        $this->db->from('empresa e');
        $this->db->join('cidade cid','cid.id=e.cidade_id');
        $this->db->join('unidade_federativa uf','cid.unidade_federativa_id=uf.id');
        
        $this->db->where('e.id', $id);
        return $this->db->get()->row();
    }

    public function retorna_empresa_ativos() {
        
        $this->db->select('e.*, cid.descricao desc_cidade, uf.id unidade_federativa_id, uf.sigla, uf.descricao desc_uf');
        $this->db->from('empresa e');
        $this->db->join('cidade cid','cid.id=e.cidade_id');
        $this->db->join('unidade_federativa uf','cid.unidade_federativa_id=uf.id');
        
        $this->db->where('e.ativo', 1);
        $this->db->order_by('e.nome_fantasia');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function existe_empresa($uf_id) {
        $this->db->from('empresa');
        $this->db->where('empresa_id', $uf_id);                
        $query = $this->db->get();
        return $query->result();
    }
    

}
