<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Serie_model class.
 * 
 * @extends CI_Model
 */
class Produto_model extends CI_Model {

    public function __construct() {
        parent::__construct();        
    }

    public function retorna_produtos() {
        
        $this->db->select('pro.*, ca.descricao desc_categoria, at.descricao desc_area, at.desc_filtro');
        $this->db->from('produto pro');
        $this->db->join('categoria ca','pro.categoria_id=ca.id');
        $this->db->join('area_atuacao at','pro.area_atuacao_id=at.id');
        $this->db->order_by('nome');
        $query = $this->db->get();
        return $query->result();
    }
    

    public function salvar($data) {
        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('produto', $data);
        } else {
            return $this->db->insert('produto', $data);
        }
    }

    public function delete($id) {
        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('produto');
        }
    }

    public function retorna_produto($id) {

        $this->db->from('produto');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_produto_ativos() {
        
        $this->db->from('produto');
        $this->db->where('ativo', 1);
        $this->db->order_by('nome');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function existe_produto($uf_id) {
        $this->db->from('produto');
        $this->db->where('produto_id', $uf_id);                
        $query = $this->db->get();
        return $query->result();
    }
    
    public function retorna_produtos_site($categoria_id) {
        
        $this->db->select('pro.*, ca.descricao desc_categoria, at.descricao desc_area, at.desc_filtro');
        $this->db->from('produto pro');
        $this->db->join('categoria ca','pro.categoria_id=ca.id');
        $this->db->join('area_atuacao at','pro.area_atuacao_id=at.id');
        $this->db->where('pro.categoria_id', $categoria_id);
        $this->db->order_by('nome');
        $query = $this->db->get();
        return $query->result();
    }

}
