<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Serie_model class.
 * 
 * @extends CI_Model
 */
class Banner_model extends CI_Model {

    public function __construct() {
        parent::__construct();        
    }

    public function retorna_banners() {
        $this->db->from('banner');
        $this->db->where('ativo',1);
        $this->db->order_by('ordem');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function salvar($data) {
        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('banner', $data);
        } else {
            return $this->db->insert('banner', $data);
        }
    }

    public function delete($id) {
        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('banner');
        }
    }

    public function retorna_banner($id) {

        $this->db->from('banner');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_banner_ativos() {
        
        $this->db->from('banner');
        $this->db->where('ativo', 1);
        $this->db->order_by('ordem');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function existe_banner($uf_id) {
        $this->db->from('banner');
        $this->db->where('banner', $uf_id);                
        $query = $this->db->get();
        return $query->result();
    }
}
  