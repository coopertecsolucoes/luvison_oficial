<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Serie_model class.
 * 
 * @extends CI_Model
 */
class Galeria_model extends CI_Model {

    public function __construct() {
        parent::__construct();        
    }

    public function retorna_galerias() {
        
        $this->db->select('ga.*, cat.descricao desc_categoria, cat.desc_filtro');
        $this->db->from('galeria ga');
        $this->db->join('categoria cat','ga.categoria_id=cat.id');
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }
    

    public function salvar($data) {
        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('galeria', $data);
        } else {
            return $this->db->insert('galeria', $data);
        }
    }

    public function delete($id) {
        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('galeria');
        }
    }

    public function retorna_galeria($id) {

        $this->db->from('galeria');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_galeria_ativos() {
        
        $this->db->from('galeria');
        $this->db->where('ativo', 1);
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function existe_galeria($uf_id) {
        $this->db->from('galeria');
        $this->db->where('galeria_id', $uf_id);                
        $query = $this->db->get();
        return $query->result();
    }
    
    public function retorna_galerias_home() {
        
        $this->db->select('ga.*, cat.descricao desc_categoria, cat.desc_filtro');
        $this->db->from('galeria ga');
        $this->db->join('categoria cat','ga.categoria_id=cat.id');
        $this->db->order_by('descricao');
        $this->db->limit('6');
        $query = $this->db->get();
        return $query->result();
    }

}
