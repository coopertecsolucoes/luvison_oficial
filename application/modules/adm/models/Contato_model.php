<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Serie_model class.
 * 
 * @extends CI_Model
 */
class Contato_model extends CI_Model {

    public function __construct() {
        parent::__construct();        
    }

      public function retorna_contatos() {
  
        $this->db->from('contato');
        $this->db->order_by('nome');
        $query = $this->db->get();
        return $query->result();
    }
    public function salvar($data) {
        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('contato', $data);
        } else {
            return $this->db->insert('contato', $data);
        }
    }

    public function delete($id) {
        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('contato');
        }
    }

    public function retorna_contato($id) {
        $this->db->from('contato');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_contato_ativos() {
        
     
        $this->db->from('contato');
        $this->db->where('ativo', 1);
        $this->db->order_by('nome');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function existe_contato($uf_id) {
        $this->db->from('contato');
        $this->db->where('nome', $uf_id);                
        $query = $this->db->get();
        return $query->result();
    }
}
  