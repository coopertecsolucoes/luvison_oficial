<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Upload_model extends CI_Model {

    public function __construct() {
        parent::__construct();        
    }

   
    function upload_file ($arquivo, $caminho, $tipo_arquivo, $nome_file){

        if(!is_dir($caminho)) {
            mkdir($caminho, 0777);
        }

        $config['upload_path'] = $caminho;
        $config['allowed_types'] = $tipo_arquivo ; //'jpg|png';
        $config['max_size'] = '100000000000';
        $config['overwrite'] = FALSE;
        $config['remove_spaces'] = TRUE;
        $new_name = normalizeChars($arquivo['name']);
        $config['file_name'] = $new_name;

        $this->load->library('upload', $config);
        
        
        if (!$this->upload->do_upload($nome_file)) {
            $error = $this->upload->display_errors();
            return "";
        } else {
            return $caminho . '/' . $this->upload->file_name;
        }


    }
}
