<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Serie_model class.
 * 
 * @extends CI_Model
 */
class Noticia_model extends CI_Model {

    public function __construct() {
        parent::__construct();        
    }

    public function retorna_noticias() {
        
        $this->db->from('noticia');
        $this->db->order_by('data');
        $query = $this->db->get();
        return $query->result();
    }
    

    public function salvar($data) {
        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('noticia', $data);
        } else {
            return $this->db->insert('noticia', $data);
        }
    }

    public function delete($id) {
        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('noticia');
        }
    }

    public function retorna_noticia($id) {

        $this->db->from('noticia');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_noticia_ativos() {
        
        $this->db->from('noticia');
        $this->db->where('ativo', 1);
        $this->db->order_by('data');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function existe_noticia($uf_id) {
        $this->db->from('noticia');
        $this->db->where('noticia_id', $uf_id);                
        $query = $this->db->get();
        return $query->result();
    }
    
    public function retorna_noticias_home() {
        
        $this->db->from('noticia');
        $this->db->where('ativo', 1);
        $this->db->limit('3');
        $this->db->order_by('data');
        $query = $this->db->get();
        return $query->result();
    }

}
