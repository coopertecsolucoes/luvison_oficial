<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Serie_model class.
 * 
 * @extends CI_Model
 */
class Categoria_model extends CI_Model {

    public function __construct() {
        parent::__construct();        
    }

    public function retorna_categorias() {
        
        $this->db->from('categoria');
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }
    

    public function salvar($data) {
        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('categoria', $data);
        } else {
            return $this->db->insert('categoria', $data);
        }
    }

    public function delete($id) {
        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('categoria');
        }
    }

    public function retorna_categoria($id) {

        $this->db->from('categoria');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_categoria_ativos() {
        
        $this->db->from('categoria');
        $this->db->where('ativo', 1);
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function existe_categoria($uf_id) {
        $this->db->from('categoria');
        $this->db->where('categoria_id', $uf_id);                
        $query = $this->db->get();
        return $query->result();
    }
    

}
