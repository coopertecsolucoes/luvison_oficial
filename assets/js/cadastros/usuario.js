/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */




    function novo(){
        $('#id').val(-1);
        $('#descricao').val("");
        $('#titulo_modal').html("Novo Usuário");
        
        $('#modal-dados').modal('show');
    }

    function editar(registro_id){ 
         
        $('#id').val(registro_id);
        $.ajax({
            url: path + "/adm/usuario/edit/" + registro_id,
            type: 'POST',            
            async: false,
            success: function (data) {
                var data = JSON.parse(data);
                var result = data.result;
                $('#descricao').val(result.descricao);
                $('#titulo_modal').html("Editar Usuário");

                $('#modal-dados').modal('show');
            }
        });
    }

    function salvar(){
        if (!valida_form("frm_modal")){
            return false;
        }
        var form = $('#frm_modal');
       
        var formData = new FormData(form[0]);
         console.log(form);
        $.ajax({
            url: path + "/adm/usuario/salvar_registro",
            type: 'POST',
            data: formData,
            async: false,
            success: function (data) {             
                carrega_registros();
                $('#modal-dados').modal('hide');
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
    
    //Recebe o array de imagnes para impressão
    function carrega_registros(){
        $('#listagem').empty();
        $('#listagem').load(path + "adm/usuario/listagem");
    }