
    function inicializa_modal(focus){
        $('#titulo_modal').html("Nova Produto de Produto");
        //Reiniciando os valores do form
        $('#id').val(-1);
        $('#categoria_id').val("-1");
        $('#area_atuacao_id').val("-1");
        $('#nome').val("");
        $('#descricao').val("");
        if(focus){
            $('form:not(.filter) :input:visible:enabled:first').focus();
        }
    }    
    
    function novo(){
        inicializa_modal(false);
        $('#modal-dados').modal('show');
         //Inicializa o curso no primeiro elemento do form
        $('form:not(.filter) :input:visible:enabled:first').focus();
    }
    
    function editar(registro_id){ 
         
        $('#id').val(registro_id);
        $.ajax({
            url: path + "/adm/produto/edit/" + registro_id,
            type: 'POST',            
            async: false,
            success: function (data) {
                var data = JSON.parse(data);
                var result = data.result;
                $('#titulo_modal').html("Editar Produto de Produto");
                $('#categoria_id').val(result.categoria_id);
                $('#area_atuacao_id').val(result.area_atuacao_id);
                $('#nome').val(result.nome);
                $('#descricao').val(result.descricao);
                marca_ativo('ativo',result.ativo);

                $('#modal-dados').modal('show');
                //Inicializa o curso no primeiro elemento do form
                $('form:not(.filter) :input:visible:enabled:first').focus();
            }
        });
    }

    function salvar(){
        if (!valida_form("frm_modal")){
            return false;
        }
        var form = $('#frm_modal');
        var formData = new FormData(form[0]);
        $.ajax({
            url: path + "/adm/produto/salvar_registro",
            type: 'POST',
            data: formData,
            async: false,
            success: function (data) {             
                carrega_registros();
                if ($("#id").val()== "-1"){
                    inicializa_modal(true);                    
                }else {
                    $('#modal-dados').modal('hide');
                }                
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
    
    //Recebe o array de imagnes para impressão
    function carrega_registros(){
        $('#listagem').empty();
        $('#listagem').load(path + "adm/produto/listagem");
    }
    
    
    function ativar_registro(registro_id) {
        var ativo = document.getElementById('ativo_' + registro_id);
        var valor = 0;
        if (ativo.checked){
            valor = 1;
        }
        $.ajax({
            url: path + "/adm/produto/ativar_registro/" + registro_id,
            type: "POST",
            datatype: "html",
            "data": {valor: valor}                     
        });
    }
    
    function excluir(registro_id){
        bootbox.confirm({
            message: 'Deseja excluir o registro?',
            buttons: {
                confirm: {
                    label: 'Sim',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Não',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if(result){
                     $.ajax({
                            url: path + "/adm/produto/delete/" + registro_id,
                            type: 'POST',
                            async: false,
                            success: function (msg) {  
                                console.log(msg)
                                if (msg != ""){
                                    bootbox.alert(msg);
                                }
                                carrega_registros();    
                                
                            },
                 
                        });
                }
                
            }
        });
        
        
    }