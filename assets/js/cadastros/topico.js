
    function inicializa_modal(focus){
        $('#titulo_modal').html("Novo Topico");
        //Reiniciando os valores do form
        $('#id').val(-1);
        $('#texto').val("");
        $('#descricao').val("");
        $('#imagem').val("");
        $('#icone').val("");
        $('#ordem').val("");
//             mostrar_avaliacao(0);
        if(focus){
            $('form:not(.filter) :input:visible:enabled:first').focus();
        }
    }    
    
    function novo(){
        inicializa_modal(false);
        $('#modal-dados').modal('show');
         //Inicializa o curso no primeiro elemento do form
        $('form:not(.filter) :input:visible:enabled:first').focus();
    }
    
    function editar(registro_id){ 
      
        $('#id').val(registro_id);
        
        $.ajax({
            url: path + "/adm/topico/edit/" + registro_id,
            type: 'POST',            
            async: false,
            success: function (data) {
                var data = JSON.parse(data);
                var result = data.result;
                var fotos = data.fotos;
                $('#texto').val(result.texto);
                $('#descricao').val(result.descricao);
                $('#imagem').val(result.imagem);
                $('#icone').val(result.icone);
                $('#ordem').val(result.ordem);     
              
//                mostrar_avaliacao(result.avaliacao);
//                
                marca_ativo('ativo',result.ativo);
               //mostrar_fotos(fotos);
//             carrega_topico_fotos();
//               
                $('#modal-dados').modal('show');
                //Inicializa o curso no primeiro elemento do form
                $('form:not(.filter) :input:visible:enabled:first').focus();
            }
        });
    }
    
//    function tab_fotos(){
//        $('#topico_id').val($("#id").val());
//    }


//    function mostrar_fotos(fotos){
//        var html = "";
//        fotos.forEach(function(val, i) {
//            //var option  = $('<option>').val( val[campo_valor]).text( val[campo_texto] ) ;
//
//            html = html + '<div class="col-lg-3 col-md-4 col-6">' 
//                + '<img class="img-fluid img-thumbnail" src="' + val["foto"] + '" alt="">'
//                + '</div>'
//        });     
//        $('#topico_fotos').html(html);
//
//    }

// function editar_foto(){
//          if (!valida_form("frm_foto")){
//            return false;
//        }
//        var form = $('#frm_foto');
//        var formData = new FormData(form[0]);
//        $.ajax({
//            url: path + "/adm/topico/upload_foto",
//            type: 'POST',
//            data: formData,
//            async: false,
//            success: function (data) {             
//                carrega_registros();
//                if ($("#id").val()== "-1"){
//                    inicializa_modal(true);                    
//                }else {
//                    $('#modal-dados').modal('hide');
//                }                
//            },
//            cache: false,
//            contentType: false,
//            processData: false
//        });
//    }
    
//    function excluir_foto(topico_foto_id){
//        bootbox.confirm({
//            message: 'Deseja excluir a foto?',
//            buttons: {
//                confirm: {
//                    label: 'Sim',
//                    className: 'btn-success'
//                },
//                cancel: {
//                    label: 'Não',
//                    className: 'btn-danger'
//                }
//            },
//            callback: function (result) {
//                if(result){
//                     $.ajax({
//                            url: path + "/adm/topico/delete_foto/" + topico_foto_id,
//                            type: 'POST',
//                            async: false,
//                            success: function (msg) {  
//                               
//                                if (msg != ""){
//                                    bootbox.alert(msg);
//                                }
//                               carrega_topico_fotos();    
//                                
//                            },
//                 
//                        });
//                }
//                
//            }
//        });
//    }         
//
//    function mostrar_avaliacao(avaliacao){
//        var html = "";
//        for(i = 1; i <= 5; i++) {
//            if(i <= avaliacao){
//                html = html + '<span class="text-blue" style="font-size: 25px;"><i class="fa fa-star"></i></span>';
//            } else {
//                html = html + '<span class="text-blue"  style="font-size: 25px;"><i class="fa fa-star-o"></i></span>';
//            }                       
//        }
//        $('#avaliacao').html(html);
//    }



    function salvar(){
        
        if (!valida_form("frm_modal")){
            return false;
        }
        var form = $('#frm_modal');
        var formData = new FormData(form[0]);
        $.ajax({
            url: path + "/adm/topico/salvar_registro",
            type: 'POST',
            data: formData,
            async: false,
            success: function (data) {             
                carrega_registros();
                if ($("#id").val()== "-1"){
                    inicializa_modal(true);                    
                }else {
                    $('#modal-dados').modal('hide');
                }                
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
    
    
    function carrega_registros(){
        $('#listagem').empty();
        $('#listagem').load(path + "adm/topico/listagem");
    }
    
    function carrega_topico_fotos(){
        var topico_id = $('#id').val();
        $('#topico_fotos').empty();
        $('#topico_fotos').load(path + "adm/topico/topico_fotos/" + topico_id);
    }
    
   
    
    
    function ativar_registro(registro_id) {
        var ativo = document.getElementById('ativo_' + registro_id);
        var valor = 0;
        if (ativo.checked){
            valor = 1;
        }
        $.ajax({
            url: path + "/adm/topico/ativar_registro/" + registro_id,
            type: "POST",
            datatype: "html",
            "data": {valor: valor}                     
        });
    }
    
    function excluir(registro_id){
        bootbox.confirm({
            message: 'Deseja excluir o registro?',
            buttons: {
                confirm: {
                    label: 'Sim',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Não',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if(result){
                     $.ajax({
                            url: path + "/adm/topico/delete/" + registro_id,
                            type: 'POST',
                            async: false,
                            success: function (msg) {  
                                console.log(msg)
                                if (msg != ""){
                                    bootbox.alert(msg);
                                }
                                carrega_registros();    
                                
                            },
                 
                        });
                }
                
            }
        });
        
        
    }