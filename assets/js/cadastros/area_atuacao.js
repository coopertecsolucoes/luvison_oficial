
    function inicializa_modal(focus){
        $('#titulo_modal').html("Nova Area de Atuação do Produto");
        //Reiniciando os valores do form
        $('#id').val(-1);
        $('#descricao').val("");
        $('#desc_filtro').val("");
        $('#ordem').val("");
        if(focus){
            $('form:not(.filter) :input:visible:enabled:first').focus();
        }
    }    
    
    function novo(){
        inicializa_modal(false);
        $('#modal-dados').modal('show');
         //Inicializa o curso no primeiro elemento do form
        $('form:not(.filter) :input:visible:enabled:first').focus();
    }
    
    function editar(registro_id){ 
         
        $('#id').val(registro_id);
        $.ajax({
            url: path + "/adm/area_atuacao/edit/" + registro_id,
            type: 'POST',            
            async: false,
            success: function (data) {
                var data = JSON.parse(data);
                var result = data.result;
                $('#titulo_modal').html("Editar Area de Atuação do Produto");
                $('#descricao').val(result.descricao);
                $('#desc_filtro').val(result.desc_filtro);
                $('#ordem').val(result.ordem);
                marca_ativo('ativo',result.ativo);

                $('#modal-dados').modal('show');
                //Inicializa o curso no primeiro elemento do form
                $('form:not(.filter) :input:visible:enabled:first').focus();
            }
        });
    }

    function salvar(){
        if (!valida_form("frm_modal")){
            return false;
        }
        var form = $('#frm_modal');
        var formData = new FormData(form[0]);
        $.ajax({
            url: path + "/adm/area_atuacao/salvar_registro",
            type: 'POST',
            data: formData,
            async: false,
            success: function (data) {             
                carrega_registros();
                if ($("#id").val()== "-1"){
                    inicializa_modal(true);                    
                }else {
                    $('#modal-dados').modal('hide');
                }                
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
    
    //Recebe o array de imagnes para impressão
    function carrega_registros(){
        $('#listagem').empty();
        $('#listagem').load(path + "adm/area_atuacao/listagem");
    }
    
    
    function ativar_registro(registro_id) {
        var ativo = document.getElementById('ativo_' + registro_id);
        var valor = 0;
        if (ativo.checked){
            valor = 1;
        }
        $.ajax({
            url: path + "/adm/area_atuacao/ativar_registro/" + registro_id,
            type: "POST",
            datatype: "html",
            "data": {valor: valor}                     
        });
    }
    
    function excluir(registro_id){
        bootbox.confirm({
            message: 'Deseja excluir o registro?',
            buttons: {
                confirm: {
                    label: 'Sim',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Não',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if(result){
                     $.ajax({
                            url: path + "/adm/area_atuacao/delete/" + registro_id,
                            type: 'POST',
                            async: false,
                            success: function (msg) {  
                                console.log(msg)
                                if (msg != ""){
                                    bootbox.alert(msg);
                                }
                                carrega_registros();    
                                
                            },
                 
                        });
                }
                
            }
        });
        
        
    }